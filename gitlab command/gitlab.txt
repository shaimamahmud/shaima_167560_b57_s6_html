Git global setup

git config --global user.name "shaima mahmud"
git config --global user.email "shaimamahmud1995@gmail.com"

Create a new repository

git clone https://shaimamahmud@gitlab.com/shaimamahmud/shaimamahmud_167560_b57_html.git
cd shaimamahmud_167560_b57_html
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://shaimamahmud@gitlab.com/shaimamahmud/shaimamahmud_167560_b57_html.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://shaimamahmud@gitlab.com/shaimamahmud/shaimamahmud_167560_b57_html.git
git push -u origin --all
git push -u origin --tags